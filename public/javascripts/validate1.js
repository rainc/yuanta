/**
 * Created with JetBrains WebStorm.
 * User: shihengwen
 * Date: 2013/11/22
 * Time: 下午 5:53
 * To change this template use File | Settings | File Templates.
 */
$(document).ready(function(){
  $("#btnSubmit").click(function(){

     if($("#txtUserName").val() == ""){
       alert("請填寫姓名！");
       $("#txtUserName").focus();
       return false;
     }else{
       if($("#txtUserName").val().length > 20){
         alert("姓名長度必須小於等於20");
         $("#txtUserName").focus();
         return false;
       }
     }

     var ident = $("#txtIdentity_ID");
     var rex = /(^\d{15}$)|(^\d{17}([0-9]|X)$)/;
     var isL = /^[A-Za-z]+$/;
     var isNum = /^[-+]?(([0-9]+)([.]([0-9]+))?|([.]([0-9]+))?)$/;
     var index = "ABCDEFGHJKLMNPQRSTUVXYWZIO";
     var array = [10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35];

     /*if(ident.val() == ""){
       alert("請填寫身分證字號！");
       ident.focus();
       return false;
     }else{
       var str = ident.val();
       var first = str.substring(0, 1);
       if(!isL.test(first)) {
         alert("身份證字號第 1 碼必須為英文字母！");
         ident.focus();
         return false;
       }
       if(str.length != 10){
         alert("台灣身份證字號共有 10 碼！");
         ident.focus();
         return false;
       }

       var toend = str.substring(1, str.length);
       if(!isNum.test(toend)) {
         alert("身份證字號第 2 ~ 10 碼必須為數字！");
         ident.focus();
         return false;
       }

       var to =  str.substring(1, 2);
       if(to != '1' && to != '2'){
         alert("身份證字號第 2 碼必須為 1 或 2");
         ident.focus();
         return false;
       }

       var end =  str.substring(9, str.length);
       var num1 = array[index.indexOf(first.toUpperCase())]  + "";      //first.charCodeAt(0)     alert(String.fromCharCode(97));
       var shi, ge;
       if(num1.length ==2){
         shi = num1.charAt(0) - 0;
         ge = num1.charAt(1) - 0;
       }else if(num1.length ==3){
         shi = num1.charAt(1) - 0;
         ge = num1.charAt(2) - 0;
       }

       var count = shi+ 9*ge + 8*(str.charAt(1) - 0) + 7*(str.charAt(2) - 0) + 6*(str.charAt(3) - 0)
       + 5*(str.charAt(4) - 0) + 4*(str.charAt(5) - 0) + 3*(str.charAt(6) - 0)+ 2*(str.charAt(7) - 0)
       + (str.charAt(8) - 0);
       var res = 10 - (count%10);
       if(end != res){
         alert("身份證字號最後一碼輸入錯誤！");
         ident.focus();
         return false;
       }
     }*/

     var isMobile=/^(?:13\d|15\d)\d{5}(\d{3}|\*{3})$/;
     var isPhone=/^((0\d{2,3})-)?(\d{7,8})(-(\d{3,}))?$/;
     var offP = $("#txtTel");

     if(offP.val() == ""){
       alert("請填寫電話(或行動電話)！");
       offP.focus();
       return false;
     }else{
       if(!isNum.test(offP.val())) {
         alert("電話號碼必須為數字！");
         ident.focus();
         return false;
       }
       if(offP.val().length != 10) {
         alert("電話號碼長度共 10 碼！");
         ident.focus();
         return false;
       }
     }

     var isEmail = /^[\w\-\.]+@[\w\-\.]+(\.\w+)+$/;
     /*var email = $("#txtEmail");
     if(email.val() == ""){
       alert("請填寫電子郵件帳號，以利HTC相關訊息通知！");
       email.focus();
       return false;
     } else{
       if(!isEmail.test(email.val())){
         alert("請正確填寫Email地址！");
         email.focus();
         return false;
       }
     }*/


    if($("#DD_nation").val() == "請選擇縣市" || $("#DD_nation").val() =="" || $("#DD_nation").val() == null){
      alert("請選擇縣市！");
      $("#DD_nation").focus();
      return false;
    }

    if($("#DD_area").val() == "請選擇鄉鎮市區" || $("#DD_area").val() =="" || $("#DD_area").val() == null){
      alert("請選擇鄉鎮市區！");
      $("#DD_area").focus();
      return false;
    }
    if($("#DD_Road").val() == "請選擇郵地區號" || $("#DD_Road").val() =="" || $("#DD_Road").val() == null){
      alert("請選擇郵地區號！");
      $("#DD_Road").focus();
      return false;
    }
    if($("#txtAddress").val() == "" || $("#txtAddress").val() ==""){
      alert("請輸入收貨地址！");
      $("#txtAddress").focus();
      return false;
    }else{
      if($("#txtAddress").val().length > 200){
        alert("收貨地址長度必須小於等於200");
        $("#txtAddress").focus();
        return false;
      }
    }

    if ($('input[name=txtPayment]')[1].checked){

      if($("#accountType").val() == ""){
        alert("請輸入轉帳帳戶銀行名稱！");
        $("#accountType").focus();
        return false;
      }else{
        if($("#accountType").val().length > 50){
          alert("轉帳帳戶銀行名稱長度必須小於等於50");
          $("#accountType").focus();
          return false;
        }
      }

      if($("#account_No").val() == ""){
        alert("請輸入轉帳帳戶後五碼！");
        $("#account_No").focus();
        return false;
      }else{
        if($("#account_No").val().length != 5){
          alert("長度必須只能是五碼！");
          $("#account_No").focus();
          return false;
        }
        if(!isNum.test($("#account_No").val())) {
          alert("五碼必須全為數字！");
          $("#account_No").focus();
          return false;
        }
      }
    }

    if ($('input[name=txtInvoice]')[1].checked){
      if($("#companyName").val() == ""){
        alert("請輸入公司名稱！");
        $("#companyName").focus();
        return false;
      }else{
        if($("#companyName").val().length > 50){
          alert("公司名稱長度必須小於等於50");
          $("#companyName").focus();
          return false;
        }
      }

      if($("#companyNo").val() == ""){
        alert("請輸入公司統一編號！");
        $("#companyNo").focus();
        return false;
      }else{

        var cx = new Array;
        cx[0] = 1;
        cx[1] = 2;
        cx[2] = 1;
        cx[3] = 2;
        cx[4] = 1;
        cx[5] = 2;
        cx[6] = 4;
        cx[7] = 1;
        //chknum();

        //function chknum(){
          var NO = $("#companyNo").val();
          var SUM = 0;
          if (NO.length != 8) {
            alert("統編錯誤，要有 8 個數字");
            $("#companyNo").focus();
            return false;
          }
          var cnum = NO.split("");
          for (i=0; i<=7; i++) {
            if (NO.charCodeAt() < 48 || NO.charCodeAt() > 57) {
              alert("統編錯誤，要有 8 個 0-9 數字組合");
              $("#companyNo").focus();
              return false;
            }
            SUM += cc(cnum[i] * cx[i]);
          }
          if (SUM % 10 == 0) ;
          else if (cnum[6] == 7 && (SUM + 1) % 10 == 0) ;
          else{
            alert("統一編號："+NO+" 錯誤!");
            $("#companyNo").focus();
            return false;
          }
        //}

        function cc(n){
          if (n > 9) {
            var s = n + "";
            n1 = s.substring(0,1) * 1;
            n2 = s.substring(1,2) * 1;
            n = n1 + n2;
          }
          return n;
        }

      }
    }
  });

});




