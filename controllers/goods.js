/**
 * Created with JetBrains WebStorm.
 * User: hongxin
 * Date: 13-1-12
 * Time: 下午5:02
 * To change this template use File | Settings | File Templates.
 */
var mysql1 = require("../models/basedao");
var config = require('../config').config;
var check = require('validator').check,
  sanitize = require('validator').sanitize;
var EventProxy = require('eventproxy').EventProxy;

var log = require('../util/log4js').logger('controllers/goods');

exports.queryGoods = function (req, res, next) {


  if (!req.session.auth) {
    res.render('Info', {PubTitle: '', PubInfo: '請login。',
      PubLocation: '/orderlist/login', PubTimeout: 1500, layout: null});

  }

  var page = parseInt(req.query.page, 10) || 1;
  var keyword = req.query || '';

  var goods_sn = req.query.goods_sn || '';
  var goods_imei = req.query.goods_imei || '';


  var limit = config.table_list_count;
  var dblimit = ' LIMIT ' + (page-1) * limit + ',' + limit;

    var events = [ 'iologs', 'pages'];
    var ep = EventProxy.create(events, function (iologs, pages) {
      res.render('goodslist/index', {
        iologs:iologs,
        current_page:page,
        pages:pages,
        keyword:keyword,
        layout: 'layout2'
      });
    });

    ep.on('error', function (err) {
      ep.unbind();
      res.render('Info', {PubTitle: '', PubInfo: err.message, PubLocation: '/goodslist', PubTimeout: 2000, layout: null});
      return;
    });

    var where = '';
    if (goods_sn.length > 0) where += ' and GOODS_SN=\'' + goods_sn + '\'';
    if (goods_imei.length > 0) where += ' and GOODS_IMEI=\'' + goods_imei + '\'';

    if (where.length > 0) where = ' where' + where.substr(4);

//    var sqls = "SELECT * FROM GoodsInfo" + where + " ORDER BY GOODS_ID DESC";
  var sqls = "SELECT a.GOODS_SN,a.GOODS_STATUS,b.USER_NAME,b.TEL FROM goodsinfo a INNER JOIN buyinfo b on a.GOODS_SN = b.GOODS_SN" + where + " ORDER BY GOODS_ID DESC";

  sqls += dblimit;

    console.log(sqls);
    mysql1.find(sqls, function (err, iologs) {
      if (err) {
        return ep.emit('error', err);
      }
      ep.emit('iologs', iologs);
    });

  sqls = 'SELECT count(*) sumsize FROM GoodsInfo' + where;
  mysql1.find(sqls, function(err,data){
    console.log(data);
    if (err) {
      return ep.emit('error', err);
    }
    var pages = Math.ceil(data[0].sumsize / limit);
    ep.emit('pages', pages);
  });
};


/**
 * 更新订单
 * */
exports.addGoods = function(req, res, next){

  log.info("############################ 添加商品資料 ######################");
  log.info("客戶端IP: "+req.connection.remoteAddress);

  var obj = req.body;
  var goods_sn = obj.goods_sn || '';
  var goods_imei = obj.goods_imei || '';
  var cdkey = obj.cdkey || '';

  log.info("傳入參數: "+ JSON.stringify(obj));

  var sqls = "SELECT count(*) count FROM GoodsInfo WHERE GOODS_SN='"+goods_sn+"' OR GOODS_IMEI='"+goods_imei+"'";
  log.info("sql = " + sqls);
  mysql1.find(sqls, function (err, results) {
    if (err) {
      log.info(err);
      return false;
    } else {
      log.info(results);
      var resJson = results[0];
      var key = resJson.count;
      if(key >= '1'){
        res.json({ID: '', Result: 2, ResultData: ''});
      }else if(key == '0'){
        try{
          var sql = "INSERT INTO GoodsInfo(GOODS_SN, CDKEY, GOODS_IMEI, GOODS_STATUS) " +
            "VALUES('"+goods_sn+"','"+cdkey+"','"+goods_imei+"','1')";
          console.log(sql);
          log.info("sql=" + sql);
          mysql1.process(sql, null);
          res.json({ID: '', Result: 0, ResultData: ''});
        }catch(e){
          log.info( "發生錯誤！"+e);
          res.json({ID: '',  Result: 1, ResultData: ''});
        }
      }
    }
   });
};




