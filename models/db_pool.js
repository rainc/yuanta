/**
 * Created with JetBrains WebStorm.
 * User: shihengwen
 * Date: 2013/10/18
 * Time: 下午 2:45
 * To change this template use File | Settings | File Templates.
 */
var mysql = require('mysql');
var config = require('../config').config;

var db_config = {
  host: config.db_host,
  user: config.db_user,
  password: config.db_passwd,
  database: config.db_database,
  port: 3306
};

// 创建连接池
var pool = mysql.createPool(db_config);
var connection;


function getPoolConn(){
   return pool;
}

function getConn(){
    return handleDisconnect();
}
function handleDisconnect() {
    connection = mysql.createConnection(db_config); // 创建连接对象

    // 连接失败，2秒重连
    connection.connect(function(err) {
        if(err) {
            console.log('error when connecting to db:', err);
            setTimeout(handleDisconnect, 2000);
        }
    });
    // 如果断线自动重连
    connection.on('error', function(err) {
        console.log('db error', err);
        if(err.code === 'PROTOCOL_CONNECTION_LOST') {
            handleDisconnect();
        } else {
            throw err;
        }
    });

    return connection;
}
//handleDisconnect();

exports.getPoolConn = getPoolConn;
exports.getConn = getConn;
