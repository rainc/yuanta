/**
 * Created with JetBrains WebStorm.
 * User: shihengwen
 * Date: 2013/12/3
 * Time: 下午 4:57
 * To change this template use File | Settings | File Templates.
 */
var mysql = require("../models/basedao");
var utils = require('../util/utils');
var xlsx = require('node-xlsx');
var fs = require('fs');
var log = require('../util/log4js').logger('controllers/excel');
var config = require('../config').config;

var myasync = require("async");

var obj = {};


/**
 * 對賬匯入
 * */
exports.payImportData = function(req, res, next){
  log.info("############################ 匯入 ######################");
  log.info("客戶端IP: "+req.connection.remoteAddress);

  try{
    var path = req.files.fileField.path || '';
    var buy_type = req.body.buy_type || '';
    log.info("傳入參數: file=" + req.body.textfield + " buy_type=" + buy_type);

    if(path){
      var obj = xlsx.parse(path);

      if(obj){
        var stat = fs.statSync(path);
        if(stat.isFile()){
          fs.unlinkSync(path);    //直接删除文件
        }
        var rootObj = obj.worksheets[0].data;
        var arrLen = 0;
        var arrCount = 0;

        // 統計成功、失敗筆數
        var okCount = 0;
        var errCount = 0;
        var ditCount = 0;
        var sql = "", upsqls = "";
        var express_no = "";
        
        var delArr = [];
        var tempArr = new Array();
        var index = 0;
        

        myasync.waterfall([
          function postOffice(_callback){
            if(buy_type == '1'){   // 貨到付款
            	 var myDit = 0;
            	 rootObj.forEach(function (arr){ 
            		 myDit++;
                     if(arr.length > 0 ) {
                       if(arr.length < 5 || myDit == 1)  {                        
                       } else {
                           if((arr[0].value + "") == "NaN" || (arr[0].value + "").indexOf("/") == -1){                           
                            } else{                             	
                              	tempArr[index] = (arr[1].value + "").split(" ")[1];
                                index++;                            	  
                            }  
                        } 
            		 }             		 
            	 });
            	 /*console.log("tempArr = " + tempArr.length);
                 if(tempArr.length == 131)
               	  console.log("tempArr = " + tempArr);*/
                 delArr = tempArr.delete(); 
                 
                 console.log("delArr = " + delArr); 
            	
            	
            	
              rootObj.forEach(function (arr){
                arrCount++;
                if(arr.length > 0 ) {
                  if(arr.length < 5 || arrCount == 1)  {
                    //return;
                    arrLen++;
                  } else {
                    if((arr[0].value + "") == "NaN" || (arr[0].value + "").indexOf("/") == -1){
                      arrLen++;
                    } else{
                      express_no = (arr[1].value + "").split(" ")[1];
                      var strNO = (express_no+"").substring(0, 6);

                      // 與BUYINFO表的(EXPRESS_NO)送貨單號比對
                      var sqls = "SELECT count(*) count,BUY_ORDERNO orderno FROM BuyInfo WHERE BUY_TYPE='"+buy_type+"' AND EXPRESS_NO='"+strNO+"'";
//                      log.info("驗證 sql = " + sqls);
                      mysql.find(sqls, function (err, results) {
                        if (err) {
                          log.info(err);
                          // return false;
                          _callback(err);
                        } else {
//                          log.info(results);
                          var resJson = results[0];
                          var key = resJson.count;
                          var orderid = resJson.orderno || '';

                          myasync.waterfall([
                            function (cb){
                              //   AND BUY_ORDER='"+orderid+"'
                              var ditSql = "SELECT count(*) count FROM PAY_ORDER WHERE EXPRESS_NO='"+(arr[1].value + "").split(" ")[1]+"'";
//                              console.log("ditSql = " + ditSql);
                              try{
                                mysql.find(ditSql,function (err, results) {
                                  cb(err, results);
                                });
                              }catch (e){}
                            }
                          ], function (err, values) {
                        	  var isDit = -1;
                              var row = values[0].count;
                             
                              for(var k=0; k<delArr.length; k++){
                            	  if((arr[1].value + "").split(" ")[1] == delArr[k]){
                            		isDit=1;
                            		break;
                            	  }
                              }                   
                                                          
                            
                            
                            if(key == '1'){
                              sql = "insert into PAY_ORDER(PAY_TIME,BUY_ORDER,EXPRESS_NO,PAY_MONEY,BUY_TYPE,PAY_ORDER_STATUS) " +
                                "values(?,'"+orderid+"', '"+(arr[1].value + "").split(" ")[1]+"', '"+arr[4].value+"','1','2')";                              
                              upsqls = "UPDATE PAY_ORDER SET BUY_ORDER='"+orderid+"',PAY_MONEY='"+arr[4].value+"',PAY_ORDER_STATUS='2' WHERE EXPRESS_NO='"+(arr[1].value + "").split(" ")[1]+"'";
                              
                              var mySql = "insert into PAY_ORDER(PAY_TIME,BUY_ORDER,EXPRESS_NO,PAY_MONEY,BUY_TYPE,PAY_ORDER_STATUS) " +
                              "values(?,'"+orderid+"', '"+(arr[1].value + "").split(" ")[1]+"', '"+arr[4].value+"','1','1')";
                              
                              myupsqls = "UPDATE PAY_ORDER SET BUY_ORDER='"+orderid+"',PAY_MONEY='"+arr[4].value+"',PAY_ORDER_STATUS='1' WHERE EXPRESS_NO='"+(arr[1].value + "").split(" ")[1]+"'";

                              // 更新PAY_STATUS、PAY_ORDER_STATUS狀態
                              var upSql = "UPDATE BuyInfo SET PAY_STATUS='2',PAY_ORDER_STATUS='2' WHERE BUY_TYPE='"+buy_type+"' AND EXPRESS_NO='"+strNO+"'";
                              mysql.process(upSql, null);
                             
                              var date = arr[0].value.split("/");
                              if(isDit == -1){
                            	  if(row == '1'){
                            		  ditCount++;                            		  
                                      mysql.process(upsqls, new Date( ( (date[0] - 0) + 1911 ) + "/" + date[1] + "/"+date[2]));
                            	  } else if(row == '0'){
                            		  okCount++;                            		  
                                      mysql.process(sql, new Date( ( (date[0] - 0) + 1911 ) + "/" + date[1] + "/"+date[2]));
                                  }  
                              }else if(isDit == 1){
                            	  if(row == '1'){                            		                        		  
                                      mysql.process(myupsqls, new Date( ( (date[0] - 0) + 1911 ) + "/" + date[1] + "/"+date[2]));
                            	  } else if(row == '0'){                            		                         		  
                                      mysql.process(mySql, new Date( ( (date[0] - 0) + 1911 ) + "/" + date[1] + "/"+date[2]));
                                  }  
                            	  ditCount++;
                              } 
                             
                              arrLen++;
                            }else if(key == '0'){
                              sql = "insert into PAY_ORDER(PAY_TIME,BUY_ORDER,EXPRESS_NO,PAY_MONEY,BUY_TYPE,PAY_ORDER_STATUS) " +
                                "values(?,'', '"+(arr[1].value + "").split(" ")[1]+"', '"+arr[4].value+"','1','3')";                              
                             
                              upsqls = "UPDATE PAY_ORDER SET PAY_MONEY='"+arr[4].value+"',PAY_ORDER_STATUS='3' WHERE EXPRESS_NO='"+(arr[1].value + "").split(" ")[1]+"'";

                              var mySql = "insert into PAY_ORDER(PAY_TIME,BUY_ORDER,EXPRESS_NO,PAY_MONEY,BUY_TYPE,PAY_ORDER_STATUS) " +
                              "values(?,'', '"+(arr[1].value + "").split(" ")[1]+"', '"+arr[4].value+"','1','1')";
                              myupsqls = "UPDATE PAY_ORDER SET PAY_MONEY='"+arr[4].value+"',PAY_ORDER_STATUS='1' WHERE EXPRESS_NO='"+(arr[1].value + "").split(" ")[1]+"'";

                              

                              var date = arr[0].value.split("/");
                              if(isDit == -1){
                            	  if(row == '1'){
                                	  ditCount++;//                                	                                	  
                                	  mysql.process(upsqls, new Date( ( (date[0] - 0) + 1911 ) + "/" + date[1] + "/"+date[2]));                                	  
                                  } else if(row == '0'){
                                	  errCount++;
                                	  mysql.process(sql, new Date( ( (date[0] - 0) + 1911 ) + "/" + date[1] + "/"+date[2]));
                                  }
                              }else if(isDit == 1){
                            	  ditCount++;
                            	  if(row == '1'){                                	                                	                                	  
                                	  mysql.process(myupsqls, new Date( ( (date[0] - 0) + 1911 ) + "/" + date[1] + "/"+date[2]));                                	  
                                  } else if(row == '0'){                                	 
                                	  mysql.process(mySql, new Date( ( (date[0] - 0) + 1911 ) + "/" + date[1] + "/"+date[2]));
                                  }
                              }
                            
                              arrLen++;
                            }else if(key != '0' && key != '1'){
                            	mySql = "insert into PAY_ORDER(PAY_TIME,BUY_ORDER,EXPRESS_NO,PAY_MONEY,BUY_TYPE,PAY_ORDER_STATUS) " +
                                "values(?,'', '"+(arr[1].value + "").split(" ")[1]+"', '"+arr[4].value+"','1','1')";

                              myupsqls = "UPDATE PAY_ORDER SET PAY_MONEY='"+arr[4].value+"',PAY_ORDER_STATUS='1' WHERE EXPRESS_NO='"+(arr[1].value + "").split(" ")[1]+"'";
                            
                                                           
                              var date = arr[0].value.split("/");
                              if(row == '1'){                                	                                	                                	  
                            	  mysql.process(myupsqls, new Date( ( (date[0] - 0) + 1911 ) + "/" + date[1] + "/"+date[2]));                                	  
                              } else if(row == '0'){                                	 
                            	  mysql.process(mySql, new Date( ( (date[0] - 0) + 1911 ) + "/" + date[1] + "/"+date[2]));
                              }                             
                              
                              ditCount++;
                              arrLen++;
                            }

                            if(arrLen == rootObj.length)
                              _callback(null,{errCount:errCount, okCount:okCount, ditCount:ditCount, delArr:delArr});
                          });
                        }
                      });
                    }

                  }
                }else{
                  console.log(arr);
                  log.info("rootObj = " + rootObj);
                }

              });

            } else if(buy_type == '2'){      // atm轉賬  1  5  7
            	
              rootObj.forEach(function (arr){
            	  if(arr.length > 0 ) {
                      if(arr.length < 8 || arrCount == 1)  {                       
                      } else {

                        if((arr[0].value + "") == "Invalid Date" || (arr[0].value + "").indexOf(":00:") == -1){                          
                        } else{
                        	var arr7 = (arr[7].value + "").split("/")[1];
                        	tempArr[index] = (arr7+"").substring(arr7.length - 5, arr7.length);
                            index++;                       
                            
                        }
                      }
            	  }
              });
              
              /*console.log("tempArr = " + tempArr.length);
              if(tempArr.length == 75)
            	  console.log("tempArr = " + tempArr);*/
              delArr = tempArr.delete();
             
              rootObj.forEach(function (arr){            	
                arrCount++;                
                if(arr.length > 0 ) {
                  if(arr.length < 8 || arrCount == 1)  {
                    arrLen++;
                  } else {

                    if((arr[0].value + "") == "Invalid Date" || (arr[0].value + "").indexOf(":00:") == -1){
                      arrLen++;
                    } else{
                      
                      var arr7 = (arr[7].value + "").split("/")[1];
//                      var accountNO = (arr7+"").substring(arr7.length - 5, arr7.length);                    
                                           

                      // 與BUYINFO表的(EXPRESS_NO)送貨單號比對
                      var sqls = "SELECT count(*) count,BUY_ORDERNO orderno FROM BuyInfo WHERE BUY_TYPE='"+buy_type+"' AND ACCOUNT_NO='"+(arr7+"").substring(arr7.length - 5, arr7.length)+"'";
                      log.info("驗證 sql = " + sqls);
                      mysql.find(sqls, function (err, results) {
                        if (err) {
                          log.info(err);
                          // return false;
                          _callback(err);
                        } else {
                          var resJson = results[0];
                          var key = resJson.count;
                          var orderid = resJson.orderno;
//                        accountNO = (arr7+"").substring(arr7.length - 5, arr7.length);                    
                          

                          myasync.waterfall([
                            function (cb){
                              var ditSql = "SELECT count(*) count FROM PAY_ORDER WHERE ACCOUNT_NO='"+(arr7+"").substring(arr7.length - 5, arr7.length)+"'";
                              console.log("ditSql = " + ditSql);
                              try{
                                mysql.find(ditSql,function (err, results) {
                                  cb(err, results);
                                });
                              }catch (e){}
                            }
                          ], function (err, values) {
                        	var isDit = -1;
                            var row = values[0].count;
                           
                            for(var k=0; k<delArr.length; k++){
                          	  if((arr7+"").substring(arr7.length - 5, arr7.length) == delArr[k]){
                          		isDit=1;
                          		break;
                          	  }
                            }
                            
                            console.log("delArr = " + delArr); 
                            if(key == '1'){

                              sql = "insert into PAY_ORDER(PAY_TIME,BUY_ORDER,ACCOUNT_NO,PAY_MONEY,BUY_TYPE,PAY_ORDER_STATUS) " +
                                "values(?,'"+orderid+"', '"+(arr7+"").substring(arr7.length - 5, arr7.length)+"', '"+arr[5].value+"','2','2')";
                              upsqls = "UPDATE PAY_ORDER SET BUY_ORDER='"+orderid+"',PAY_MONEY='"+arr[5].value+"',PAY_ORDER_STATUS='2' WHERE ACCOUNT_NO='"+(arr7+"").substring(arr7.length - 5, arr7.length)+"'";

                              var mySql = "insert into PAY_ORDER(PAY_TIME,BUY_ORDER,ACCOUNT_NO,PAY_MONEY,BUY_TYPE,PAY_ORDER_STATUS) " +
                              	"values(?,'"+orderid+"', '"+(arr7+"").substring(arr7.length - 5, arr7.length)+"', '"+arr[5].value+"','2','1')";
                              myupsqls = "UPDATE PAY_ORDER SET BUY_ORDER='"+orderid+"',PAY_MONEY='"+arr[5].value+"',PAY_ORDER_STATUS='1' WHERE ACCOUNT_NO='"+(arr7+"").substring(arr7.length - 5, arr7.length)+"'";

                              // 更新PAY_STATUS、PAY_ORDER_STATUS狀態
                              var upSql = "UPDATE BuyInfo SET PAY_STATUS='2',PAY_ORDER_STATUS='2' WHERE BUY_TYPE='"+buy_type+"' AND ACCOUNT_NO='"+(arr7+"").substring(arr7.length - 5, arr7.length)+"'";
                              mysql.process(upSql, null);
                              
                              if(isDit == -1){
                            	  if(row == '1'){
                            		  ditCount++;
                            		  mysql.process(upsqls, new Date(arr[1].value));
                            	  } else if(row == '0'){
                            		  okCount++;
                            		  mysql.process(sql, new Date(arr[1].value));
                                  }  
                              }else if(isDit == 1){
                            	  ditCount++; 
                            	  if(row == '1'){                            		 
                            		  mysql.process(myupsqls, new Date(arr[1].value));
                            	  } else if(row == '0'){                            		 
                            		  mysql.process(mySql, new Date(arr[1].value));
                                  }  
                              }                                
                             
                              arrLen++;
                            }else if(key == '0'){
                              sql = "insert into PAY_ORDER(PAY_TIME,BUY_ORDER,ACCOUNT_NO,PAY_MONEY,BUY_TYPE,PAY_ORDER_STATUS) " +
                                "values(?,'', '"+(arr7+"").substring(arr7.length - 5, arr7.length)+"', '"+arr[5].value+"','2','3')";
                              upsqls = "UPDATE PAY_ORDER SET PAY_MONEY='"+arr[5].value+"',PAY_ORDER_STATUS='3' WHERE ACCOUNT_NO='"+(arr7+"").substring(arr7.length - 5, arr7.length)+"'";
                              
                              var mySql = "insert into PAY_ORDER(PAY_TIME,BUY_ORDER,ACCOUNT_NO,PAY_MONEY,BUY_TYPE,PAY_ORDER_STATUS) " +
                              	"values(?,'', '"+(arr7+"").substring(arr7.length - 5, arr7.length)+"', '"+arr[5].value+"','2','1')";
                              
                              myupsqls = "UPDATE PAY_ORDER SET PAY_MONEY='"+arr[5].value+"',PAY_ORDER_STATUS='1' WHERE ACCOUNT_NO='"+(arr7+"").substring(arr7.length - 5, arr7.length)+"'";
                              
                              if(isDit == -1){
                            	  if(row == '1'){
                                	  ditCount++;//                                	                                 	  
                                	  mysql.process(upsqls, new Date(arr[1].value));                                	  
                                  } else if(row == '0'){
                                	  errCount++;
                                	  mysql.process(sql, new Date(arr[1].value));
                                  }
                              }else if(isDit == 1){
                            	  ditCount++;  
                            	  if(row == '1'){                            		 
                            		  mysql.process(myupsqls, new Date(arr[1].value));
                            	  } else if(row == '0'){                            		 
                            		  mysql.process(mySql, new Date(arr[1].value));
                                  }  
                              }  
                                                            
                              arrLen++;
                            }else if(key != '0' && key != '1'){

                            	mySql = "insert into PAY_ORDER(PAY_TIME,BUY_ORDER,ACCOUNT_NO,PAY_MONEY,BUY_TYPE,PAY_ORDER_STATUS) " +
                                "values(?,'', '"+(arr7+"").substring(arr7.length - 5, arr7.length)+"', '"+arr[5].value+"','2','1')";


                              myupsqls = "UPDATE PAY_ORDER SET PAY_MONEY='"+arr[5].value+"',PAY_ORDER_STATUS='1' WHERE ACCOUNT_NO='"+(arr7+"").substring(arr7.length - 5, arr7.length)+"'";
                                                     	   
                        	  if(row == '1'){                            		 
                        		  mysql.process(myupsqls, new Date(arr[1].value));
                        	  } else if(row == '0'){                            		 
                        		  mysql.process(mySql, new Date(arr[1].value));
                              }  
                              ditCount++;
                              arrLen++;
                            }
                            if(arrLen == rootObj.length)
                              _callback(null,{errCount:errCount, okCount:okCount, ditCount:ditCount, delArr:delArr});

                          });
                        }
                      });
                    }
                  }
                }else{
                  console.log(arr);
                  log.info("rootObj = " + rootObj);
                }
              });
            }
          }
        ], function(err, values) {
          if(!err){
            console.log("成功：" + okCount +"失敗：" + errCount);
                        /*req.session.PubInfo="汇入完成。成功筆數: "+okCount+" ;失敗筆數: " + errCount + "。";
                        res.redirect('/payorderlist');*/

            res.render('Info', {PubTitle: '', PubInfo: "匯入成功。成功筆數: "+values.okCount+" ;失敗筆數: " + values.errCount 
            	+ " ;重複筆數: " +values.ditCount + " ，（ " + delArr + "）。",
              PubLocation: '/payorderlist', PubTimeout: 5500, layout: null});
          }else{
            log.info("err = " + err);
            res.render('Info', {PubTitle: '', PubInfo: "匯入错误。成功筆數: "+values.okCount+" ;失敗筆數: " + values.errCount + " ;重複筆數: " +values.ditCount + "。",
              PubLocation: '/payorderlist', PubTimeout: 4500, layout: null});
          }
        });
      }
    }
  }catch (e){
    log.info("發送錯誤：" + e);
    console.log(e);
  }
};




/**
 * 訂單匯出報表
 * */
exports.orderImportExcel = function(req, res, next){
  var form = req.query || '';
  var pay_status = form.pay_status || '';
  var dates = form.dates || '';

  log.info("############################ 訂單匯出報表 ######################");
  log.info("客戶端IP: "+req.connection.remoteAddress);
  log.info("傳入參數: pay_status="+pay_status+" ### dates=" +dates);

  var where = '';
  if (dates.length > 0) where += ' and DATE_FORMAT(BUY_TIME,\'%Y/%m/%d\')=\'' + dates + '\'';
  if (pay_status.length > 0) where += ' and BUY_TYPE=\'' + pay_status + '\'';

  if (where.length > 0) where = ' where' + where.substr(4);

  var sqls = "SELECT * FROM BuyInfo" + where;

  console.log("sql = " + sqls);
  mysql.find(sqls, function (err, results) {
    if (err) {
      console.log(err);
      return false;
    } else {
      console.log("匯出 results.length=" + results.length);
      obj = {
        worksheets: [
          {
            "name":"訂單報表",
            "data":[
              config.sheet
            ]
          }
        ]
      };
      var rootObj = obj.worksheets[0].data;
      for(var i=0; i<results.length; i++){
        rootObj[i+1] = createArray(results[i]);
      }
      try{
        var path = /*utils.dateFormat("yyyyMMddhhmmss")+*/'orderImportExcel.xlsx';
        log.info("path = " + path);
        var file = xlsx.build(obj);
        //fs.writeFileSync(path, file, "binary");
        // r w a
        fs.writeFileSync(path, file, {encoding: 'utf8', flags: 'w', autoClose: true});
        res.download(path, utils.dateFormat("yyyyMMddhhmmss")+'.xlsx');


      }catch(e){
        console.log(e);
        log.info("發生錯誤：" + e);
      }
    }
  });
};

function createArray(res){
  try{
    var arr =  [];
    arr[0] = {"value":res.USER_NAME,"formatCode":"General"};
    arr[1] = {"value":res.TEL,"formatCode":"0000000000"};
    arr[2] = {"value":res.EMAIL + "","formatCode":"General"};
    arr[3] = {"value":res.GOODS_SN + "","formatCode":"000000000000"};
    arr[4] = {"value":res.GOODS_IMEI + "","formatCode":"000000000000000"};

    var buy_type = res.BUY_TYPE;
    if(buy_type == "1"){
      arr[5] = {"value":"貨到付款","formatCode":"General"};
    } else if(buy_type == "2") {
      arr[5] = {"value":"ATM轉帳","formatCode":"General"};
    }else if(buy_type == "3") {
        arr[5] = {"value":"WebATM轉帳","formatCode":"General"};
    }else{
      arr[5] = {"value":"","formatCode":"General"};
    }

    var invoice_type = res.INVOICE_TYPE;
    if(invoice_type == "1"){
      arr[6] = {"value":"二聯發票","formatCode":"General"};
    } else if(invoice_type == "2") {
      arr[6] = {"value":"三聯發票","formatCode":"General"};
    } else{
      arr[6] = {"value":"","formatCode":"General"};
    }

    arr[7] = {"value":res.ACCOUNT_NO + "","formatCode":"00000"};

    var buy_time = res.BUY_TIME;
    if(buy_time){
      arr[8] ={"value":utils.dateFormat1(res.BUY_TIME, "yyyy/MM/dd")  + "","formatCode":"General"};
    }
    else{
      arr[8] = {"value":"","formatCode":"General"};
    }

    arr[9] = {"value":res.COMPANY_NAME + "","formatCode":"General"};
    arr[10] = {"value":res.COMPANY_NO + "","formatCode":"00000000"};

    var address = res.BUY_ADDRESS;
    arr[11] = {"value":address.substring(3, address.length) + "","formatCode":"General"};
    arr[12] = {"value":address.substring(0, 3) + "","formatCode":"000"};

    var buy_status = res.BUY_STATUS;
    if(buy_status == "1"){
      arr[13] = {"value":"未出貨","formatCode":"General"};
    } else if(buy_status == "2") {
      arr[13] = {"value":"已出貨","formatCode":"General"};
    } else{
      arr[13] = {"value":"","formatCode":"General"};
    }

    var expresss_no = res.EXPRESS_NO;
    var format = "";
    if(expresss_no){
      for(var i=0; i<expresss_no.length; i++)
        format += "0";
      arr[14] = {"value":expresss_no,"formatCode":format};
    } else{
      arr[14] = {"value":"","formatCode":"General"};
    }

    var express_time = res.EXPRESS_TIME;
    if(express_time){
      if(express_time == '0000-00-00 00:00:00') {
        arr[15] = {"value":"","formatCode":"General"};
      } else{
        arr[15] = {"value":utils.dateFormat1(express_time, "yyyy/MM/dd") + "","formatCode":"General"};
      }
    }
    else{
      arr[15] = {"value":"","formatCode":"General"};
    }

    arr[16] = {"value":res.INVOICE_NO + "","formatCode":"0000000000"};

    var pay_status = res.PAY_STATUS;
    if(pay_status == "1"){
      arr[17] = {"value":"未收到","formatCode":"General"};
    } else if(pay_status == "2") {
      arr[17] = {"value":"已收到","formatCode":"General"};
    }else{
      arr[17] = {"value":"","formatCode":"General"};
    }

    arr[18] = {"value":res.BUY_ORDERNO + "","formatCode":"000000000000"};
    arr[19] = {"value":res.ACCOUNT_TYPE + "","formatCode":"General"};

    var order_status = res.ORDER_STATUS;
    if(order_status == "1"){
      arr[20] = {"value":"成功訂購","formatCode":"General"};
    } else if(order_status == "2") {
      arr[20] = {"value":"取消訂單","formatCode":"General"};
    }  else if(order_status == "3") {
      arr[20] = {"value":"退貨","formatCode":"General"};
    } else if(order_status == "4") {
      arr[20] = {"value":"退款","formatCode":"General"};
    } else if(order_status == "5") {
      arr[20] = {"value":"退貨退款","formatCode":"General"};
    }else if(order_status == "6") {
        arr[20] = {"value":"訂購失敗","formatCode":"General"};
    } else{
      arr[20] = {"value":"","formatCode":"General"};
    }
    arr[21] = {"value":res.REMARK + "","formatCode":"General"};

//    console.log(arr);
    return arr;
  }catch (e){
    console.log(e);
    log.info("發生錯誤：" + e);
  }
}

Array.prototype.delete = function() {
	var arr = this;
	var obj = {};
	var delArr = [];
	for (var i = 0, l=arr.length; i < l;)
	{
	var key = arr[i];
	if (typeof obj[key] == 'undefined')
	{ 
	obj[key] = "1"; 
	i++; 
	continue;
	}
	delArr.push(arr.splice(i, 1)[0]);
	var l = arr.length;
	} 
	console.log(delArr);
	return delArr;
};




/**
 * 二次對賬
 * */
exports.twoPay = function(req, res, next){

    log.info("############################ 二次對賬 ######################");
    log.info("客戶端IP: "+req.connection.remoteAddress);
    try{
        myasync.waterfall([
            function (cb){
                mysql.process("UPDATE  PAY_ORDER p, BuyInfo b SET p.BUY_ORDER=b.BUY_ORDERNO, b.PAY_STATUS='2', p.PAY_ORDER_STATUS='2', b.PAY_ORDER_STATUS='2'"
                    +" where  b.BUY_TYPE='1' and  b.BUY_ORDERNO IS NOT NULL and b.BUY_ORDERNO <> '' and p.PAY_ORDER_STATUS  IN('1', '3') and  p.EXPRESS_NO"
                    +" IS NOT NULL and p.EXPRESS_NO <> '' and LEFT(p.EXPRESS_NO,6)=b.EXPRESS_NO", null);

                mysql.process("UPDATE  PAY_ORDER p, BuyInfo b SET p.BUY_ORDER=b.BUY_ORDERNO, b.PAY_STATUS='2', p.PAY_ORDER_STATUS='2', b.PAY_ORDER_STATUS='2'"
                    +" where  b.BUY_TYPE='2' and  b.BUY_ORDERNO IS NOT NULL and b.BUY_ORDERNO <> '' and p.PAY_ORDER_STATUS  IN('1', '3') and  p.ACCOUNT_NO"
                    +" IS NOT NULL and p.ACCOUNT_NO <> '' and p.ACCOUNT_NO=b.ACCOUNT_NO", null);

                cb();
            } ],
            function () {
               res.json({ID: '',  Result: 0, ResultData: ''});
        });
    }catch(e){
        log.info( "發生錯誤！"+e);
        res.json({ID: '',  Result: 1, ResultData: ''});
    }
};







