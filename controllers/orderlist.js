﻿/**
 * Created with JetBrains WebStorm.
 * User: hongxin
 * Date: 13-1-12
 * Time: 下午5:02
 * To change this template use File | Settings | File Templates.
 */

var mysql1 = require("../models/basedao");
var config = require('../config').config;
//var i18n = config.i18n;
var check = require('validator').check,
  sanitize = require('validator').sanitize;
//var Status = require('../status');
var EventProxy = require('eventproxy').EventProxy;

var log = require('../util/log4js').logger('controllers/order');

var mail_ctrl = require("../models/mail");

exports.index = function (req, res, next) {

  if (!req.session.auth) {
    try{
      res.render('Info', {PubTitle: '', PubInfo: '請login。',
        PubLocation: '/orderlist/login', PubTimeout: 1500, layout: null});
    }catch (e){}
  }

  var page = parseInt(req.query.page, 10) || 1;
  var keyword = req.query || '';

  var orderNo = req.query.orderNo || '';
  var userName = req.query.userName || '';
  var buy_status = req.query.buy_status || '';
  var dates = req.query.dates || '';

  var pay_status = req.query.pay_status || '';
  var account_no = req.query.account_no || '';
  var goods_sn = req.query.goods_sn || '';
  var expressno = req.query.expressno || '';

  // 2013-12-16
  var payorderstatus = req.query.payorderstatus || '';



  var limit = config.table_list_count;
  var dblimit = ' LIMIT ' + (page-1) * limit + ',' + limit;

  var action = req.query.action || '';

  if (action== 'querybyid') {
    var id = req.query.id || '';
    query_one(id);
  } else {

    var events = [ 'iologs', 'pages'];
    var ep = EventProxy.create(events, function (iologs, pages) {
      res.render('orderlist/index', {
        iologs:iologs,
        current_page:page,
        pages:pages,
        keyword:keyword,
        layout: 'layout2'
      });
    });

    ep.on('error', function (err) {
      ep.unbind();
      res.render('Info', {PubTitle: '', PubInfo: err.message, PubLocation: '/orderlist', PubTimeout: 2000, layout: null});
      return;
    });

    var where = '';
    if (orderNo.length > 0) where += ' and BUY_ORDERNO=\'' + orderNo + '\'';
    if (userName.length > 0) where += ' and USER_NAME=\'' + userName + '\'';
    if (buy_status.length > 0) where += ' and BUY_STATUS=\'' + buy_status + '\'';
    if (dates.length > 0) where += ' and DATE_FORMAT(BUY_TIME,\'%Y/%m/%d\')=\'' + dates + '\'';
    if (pay_status.length > 0) where += ' and BUY_TYPE=\'' + pay_status + '\'';
    if (account_no.length > 0) where += ' and ACCOUNT_NO=\'' + account_no + '\'';
    if (goods_sn.length > 0) where += ' and GOODS_SN=\'' + goods_sn + '\'';
    if (expressno.length > 0) where += ' and EXPRESS_NO=\'' + expressno + '\'';
    if (payorderstatus.length > 0) where += ' and PAY_ORDER_STATUS=\'' + payorderstatus + '\'';

    if (where.length > 0) where = ' where' + where.substr(4);

    /**
     * STR_TO_DATE(@dt,'%c/%d/%Y');
     * DATE_FORMAT(BUY_TIME,'%Y/%c/%d')
     * */
    var sqls = "SELECT BUY_ORDERNO, DATE_FORMAT(BUY_TIME,'%Y/%m/%d') BUY_TIME, USER_NAME, TEL, ACCOUNT_TYPE," +
      "BUY_TYPE, ACCOUNT_NO, BUY_ADDRESS,EMAIL, GOODS_SN, GOODS_IMEI, BUY_TYPE, INVOICE_TYPE, ACCOUNT_NO, COMPANY_NAME, " +
      "COMPANY_NO, BUY_ADDRESS, BUY_STATUS, EXPRESS_NO, DATE_FORMAT(EXPRESS_TIME,'%Y/%m/%d') EXPRESS_TIME, INVOICE_NO, PAY_STATUS,ORDER_STATUS, BUY_ID,PAY_ORDER_STATUS FROM buyinfo" + where  + " ORDER BY BUY_ID DESC";
    sqls += dblimit;
    /*mysql1.find(sqls, function (err, results) {
     console.log(results);
     ep.emit('iologs', results);
     }) ;*/

    console.log(sqls);
    mysql1.find(sqls, function (err, iologs) {
      if (err) {
        return ep.emit('error', err);
      }
      // console.log(iologs);
      ep.emit('iologs', iologs);
    });

    sqls = 'SELECT count(*) sumsize FROM buyinfo' + where;
    mysql1.find(sqls, function(err,data){
      if (err) {
        return ep.emit('error', err);
      }
      var pages = Math.ceil(data[0].sumsize / limit);
      ep.emit('pages', pages);
    });
  }

  function query_one(id) {
    sqls = "SELECT BUY_ID, DATE_FORMAT(BUY_TIME,'%Y/%m/%d') BUY_TIME, BUY_ORDERNO, USER_NAME, TEL, GOODS_SN, GOODS_IMEI, " +
      "BUY_TYPE, ACCOUNT_NO, INVOICE_TYPE, COMPANY_NAME, COMPANY_NO, BUY_ADDRESS, BUY_TIME," +
      "BUY_STATUS, EXPRESS_NO, DATE_FORMAT(EXPRESS_TIME,'%Y/%m/%d') EXPRESS_TIME, INVOICE_NO, PAY_STATUS, EMAIL, ACCOUNT_TYPE, ORDER_STATUS, REMARK,PAY_ORDER_STATUS  FROM buyinfo where BUY_ID='"+id+"'";
    console.log(sqls);
    mysql1.find(sqls, function (err, results) {
      if (err) {
        console.log('查询出错。'+err);
        log.info('查询出错。'+err);
        return;
      }
      console.log(results);
        try{
        res.json({ID: id,  Result: 0, ResultData: results});
      }catch (e){}

    });
  }

};


/**
 * 更新订单
 * */
exports.updateOrder = function(req, res, next){

  log.info("############################ 更新購買資料 ######################");
  log.info("客戶端IP: "+req.connection.remoteAddress);

  var obj = req.body;
  var buyno = obj.buyno || '';
  var buystatus = obj.buystatus || '';
  var pay_stauts = obj.pay_stauts || '';
  var express_no = obj.express_no || '';
  var express_time = obj.express_time || '';
  var invoice_no = obj.invoice_no || '';
  var username = obj.user_name || '';
  var address = obj.buy_address || '';

  var order_status = obj.order_status || '';
  var remark = obj.remark || '';
  var buy_type = obj.buypaytype || '';

  var pay_order_status = obj.pay_order_status || '';


  if(express_no && invoice_no && express_time){
    buystatus = '2';
    if(buy_type == '2'){
      pay_stauts = '2';
    }
  } else{
    buystatus = '1';
  }

  log.info("傳入參數: "+ JSON.stringify(obj));
  try{
      var sql = "UPDATE BuyInfo SET USER_NAME='"+username+"',BUY_ADDRESS='"+address+"'," +
        "BUY_STATUS='"+buystatus+"',EXPRESS_NO='"+express_no+"',PAY_ORDER_STATUS='"+pay_order_status+"'," +
        "ORDER_STATUS='"+order_status+"',REMARK='"+remark+"'," +
        "INVOICE_NO='"+invoice_no+"',PAY_STATUS='"+pay_stauts+"',EXPRESS_TIME=? WHERE BUY_ID='"+buyno+"'";
      log.info( "sql="+sql);
      console.log( "sql="+sql);
      if(express_time){
        try{
          mysql1.process(sql, new Date(express_time));
        }catch (e){}
      }else{
        mysql1.process(sql, '0000-00-00 00:00:00');
      }
      res.json({ID: buyno, Result: 0, ResultData: ''});
  }catch(e){
    log.info( "發生錯誤！"+e);
    res.json({ID: buyno,  Result: 1, ResultData: ''});
  }
};


/**
 * 删除订单
 * */
exports.delOrder = function(req, res, next){

  log.info("############################ 刪除購買資料 ######################");
  log.info("客戶端IP: "+req.connection.remoteAddress);

  var id = req.query.id || '';
  log.info("傳入參數: buy_id = "+ id);
  try{
     if(id){
       var sql = "DELETE FROM BuyInfo WHERE BUY_ID='"+id+"'";
       mysql1.process(sql, null);
       res.json({ID: id, Result: 0, ResultData: ''});
     }
  }catch(e){
    res.json({ID: id, Result: 1, ResultData: ''});
    log.info( "發生錯誤！"+e);
  }
};


/**
 * 發送催款mail
 * */
exports.sendMail = function(req, res, next){

  log.info("############################ 發送催款mail ######################");
  log.info("客戶端IP: "+req.connection.remoteAddress);

  var obj = req.body;
  var email = obj.email || '';
  var order_id = obj.order_id || '';
  var account_no = obj.account_no || '';
  var account_type = obj.account_type || '';
  var username = obj.user_name || '';
  var address = obj.buy_address || '';


  log.info("傳入參數: "+ JSON.stringify(obj));
  try{
    if((email).trim() != ""){
      var json = {email: email, order_id: order_id, username: username, account: account_no, account_type: account_type,
        address: address};
      mail_ctrl.send_reset_pass_mail_(json, function(err,success) {
        if(err)
          log.info("Mail發送錯誤："+err);
      });
      res.json({ID: '', Result: 0, ResultData: ''});
    }
  }catch(e){
    log.info( "發生錯誤！"+e);
    res.json({ID: '', Result: 1, ResultData: ''});
  }
};



/**
 * 查询订单登陆
 * */
exports.loginQuery = function(req, res, next){
  var form = req.body || '';
  var account = form.txtAccount;
  var password = form.txtPassword;

  var sqls = "SELECT count(*) count FROM Account WHERE ACCOUNT='"+account+"' AND PASSWORD='"+password+"'";
  console.log("sql = " + sqls);
  mysql1.find(sqls, function (err, results) {
    if (err) {
      console.log(err);
      return false;
    } else {
      console.log(results);
      var count = results[0].count;
      if(count == "1"){
        req.session.auth=true;
        res.redirect('/orderlist');
      }else{
        res.render('Info', {PubTitle: '', PubInfo: '用戶名或密碼錯誤，請重新輸入。',
          PubLocation: '/orderlist/login', PubTimeout: 1500, layout: null});
      }
    }
  });

};
