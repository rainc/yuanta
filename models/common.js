
var logger = require('../util/logger').getLogger();

mysql = require('mysql-native');
var config = require('../config').config;
var i18n = config.i18n;

var db = null;

var mysqlConnect = function() {
  db = mysql.createTCPClient(config.db_host);
  //db = mysql.createTCPClient();
  //db.verbose = true;
  db.set('auto_prepare', true)
    .set('row_as_hash', false)
    .auth(config.db_database, config.db_user, config.db_passwd);
};

var findOne = function (sqls, cb) {
  db = createConnection();
  var users = null;
  //var error = null;
  var timer = null;

  db.query(sqls).on('error', function(err) {
//    error = err;
    timer && clearTimeout(timer);
    logger.error('DB: findOne on error! sql: ' + sqls);
    if (cb) cb(err);
  }).on('row', function(r) {
      users = r;
    }).on('end', function() {
      timer && clearTimeout(timer);
      if (cb) cb(null, users);
    });

  timer = setTimeout(function() {
    cb({message: i18n.__('db_timeout')}); //DB操作逾时!
    cb = null;
  }, 5000); // timeout in 5 second

};

var find = function (sqls, cb) {
  db = createConnection();
  var datas = null;
  //var error = null;
  var timer = null;

  db.query(sqls).on('error', function(err) {
//    error = err;
    timer && clearTimeout(timer);
    logger.error('DB: find on error! sql: ' + sqls);
    if (cb) cb(err);
  }).on('end', function(r) {
      timer && clearTimeout(timer);
      datas = r.result.rows;
      if (cb) cb(null, datas);
    });

  timer = setTimeout(function() {
    cb({message: i18n.__('db_timeout')});
    cb = null;
  }, 5000); // timeout in 5 second
};

var insertOne = function (sqls, cb) {
  db = createConnection();
  //var error = null;
  var data = {};
  var timer = null;

  db.query(sqls).on('error', function(err) {
    timer && clearTimeout(timer);
    logger.error('DB: insertOne on error! sql: ' + sqls);
    if (cb) cb(err);
  }).on('end', function() {
      var insert_id = this.result.insert_id;
      if (insert_id > 0 ) {
        data['insert_id'] = insert_id;
      }
      timer && clearTimeout(timer);
      if (cb) cb(null, data);
    });

  timer = setTimeout(function() {
    cb({message: i18n.__('db_timeout')});
    cb = null;
  }, 5000); // timeout in 5 second
};

var updateOne = function (sqls, cb) {
  db = createConnection();
  //var error = null;
  var data = {};
  var timer = null;
  timer = setTimeout(function() {
    cb({message: i18n.__('db_timeout')});
    cb = null;
  }, 5000); // timeout in 5 second

  db.query(sqls).on('error', function(err) {
    timer && clearTimeout(timer);
    logger.error('DB: updateOne on error! sql: ' + sqls);
    if (cb) cb(err);
  }).on('end', function() {
      timer && clearTimeout(timer);
      data['affected_rows'] = this.result.affected_rows;
      if (cb) cb(null, data);
    });
};

var updExecute = function (sqls, params, cb) {
  db = createConnection();
  //var error = null;
  var data = {};
  var timer = null;

  db.execute(sqls, params).on('error', function(err) {
    timer && clearTimeout(timer);
    if (cb) cb(err);
  }).on('end', function() {
      var insert_id = this.result.insert_id;
      if (insert_id > 0 ) {
        data['insert_id'] = insert_id;
      }
      timer && clearTimeout(timer);
      if (cb) cb(null, data);
    });

  timer = setTimeout(function() {
    cb({message: i18n.__('db_timeout')});
    cb = null;
  }, 5000); // timeout in 5 second
};

var findOneExecute = function (sqls, params, cb) {
  db = createConnection();
  var users = null;
  //var error = null;
  var timer = null;

  db.execute(sqls, params).on('error', function(err) {
//    error = err;
    timer && clearTimeout(timer);
    if (cb) cb(err);
  }).on('row', function(r) {
      users = r;
    }).on('end', function() {
      timer && clearTimeout(timer);
      if (cb) cb(null, users);
    });

  timer = setTimeout(function() {
    cb({message: i18n.__('db_timeout')});
    cb = null;
  }, 5000); // timeout in 5 second

};

var createConnection  = function() {
  if (!db || !db.connection.writable) {
    mysqlConnect();

    try{
      db.query("SET character_set_connection=utf8, character_set_results=utf8, character_set_client=binary");
      db.query('SET autocommit=1;');
    }catch(e){
    }
  }
  return db;
};

var beginTransaction = function(cb) {
//  db.query('SET autocommit=0;');
//  db.query('BEGIN');
  find('SET autocommit=0;', function (dberr, data) {
    if (dberr) {
      cb(dberr);
    } else {
      find('BEGIN', function (dberr, data) {
        if (dberr) {
          db.query('SET autocommit=1;');
          cb(dberr);
        } else {
          cb();
        }
      });
    }
  });
};

var endTransaction = function(commit, cb) {
  if (commit) {
//    db.query('COMMIT');
//    db.query('SET autocommit=1;');
    find('COMMIT', function (dberr, data) {
      if (dberr) {
        find('SET autocommit=1;', function (dberr, data) {
          if (dberr) {
            db.query('SET autocommit=1;');
            cb(dberr);
          } else {
            cb();
          }
        });
        cb(dberr);
      } else {
        find('SET autocommit=1;', function (dberr, data) {
          if (dberr) {
            db.query('SET autocommit=1;');
            cb(dberr);
          } else {
            cb();
          }
        });
      }
    });
  } else {
//    db.query('ROLLBACK');
//    db.query('SET autocommit=1;');
    find('ROLLBACK', function (dberr, data) {
      if (dberr) {
        find('SET autocommit=1;', function (dberr, data) {
          if (dberr) {
            db.query('SET autocommit=1;');
            cb(dberr);
          } else {
            cb();
          }
        });
        cb(dberr);
      } else {
        find('SET autocommit=1;', function (dberr, data) {
          if (dberr) {
            db.query('SET autocommit=1;');
            cb(dberr);
          } else {
            cb();
          }
        });
      }
    });
  }
};

//实例：
//var param = {tableName: '', fields: [], keys:[] };
//param.fields.push({name: 'user_is_lock', value: 1});
//tableUpdate(para, function(dberr, data) {
var tableUpdate = function(param, cb) {
  var err = {};

  var sqlarr = [];
  sqlarr.push('UPDATE ');
  sqlarr.push(param.tableName);
  sqlarr.push(' SET ');
  param.fields.forEach(function (field){
    sqlarr.push(',');
    sqlarr.push(field.name);
    sqlarr.push('=');
    sqlarr.push(field.value);
  });
  param.keys.forEach(function (field){
    sqlarr.push(' and ');
    sqlarr.push(field.name);
    sqlarr.push('=');
    sqlarr.push(field.value);
  });
  sqlarr.splice(sqlarr.indexOf(' and '), 1, " where ");

  if (sqlarr.length > 0)
  {
    sqlarr[3] = ' '; //第一个逗号

    sqls = sqlarr.join('');
    updateOne(sqls, function (dberr, data) {
      if (dberr) {
        err['message'] = param.tableName + ' update err:' + dberr;
        cb(err);
      } else {
//        if (data['affected_rows'] === 1) {
        cb();
//        } else {
//          err['message'] = '当前密码不正确';
//          cb(err);
//        }
      }
    });
  }
};


var DBLog = function(companyid, io, phoneno, content, cb) {
  if (content.length > 4000) {
    content = content.substr(0, 4000);
  }
  var sqls;
  sqls = 'INSERT INTO iolog SET companyid=\'' + companyid + '\', type=\'' + io + '\', phoneno=\'' + phoneno
    + '\', content=\'' + content + '\'';
  insertOne(sqls, function (err, data) {
    cb(err);
  });
};

module.exports.createConnection = createConnection;
exports.findOne = findOne;
exports.find = find;
exports.insertOne = insertOne;
exports.updateOne = updateOne;
exports.DBLog = DBLog;

exports.updExecute = updExecute;
exports.findOneExecute = findOneExecute;

exports.beginTransaction = beginTransaction;
exports.endTransaction = endTransaction;
exports.tableUpdate = tableUpdate;
