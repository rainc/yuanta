/*!
 * nodeclub - route.js
 * Copyright(c) 2012 fengmk2 <fengmk2@gmail.com>
 * MIT Licensed
 */

/**
 * Module dependencies.
 */

var api = require('./controllers/app');
var orderListAp = require('./controllers/orderlist');
var excel = require('./controllers/excel');
var goodsListAp = require('./controllers/goods');
var print = require('./controllers/atmprint');
var payListAp = require('./controllers/payorder');
//var virtual = require('./controllers/virtual');

module.exports = function (app) {
  // home page
//  app.get('/', site.index);
  app.get('/', function (req, res) {
    //res.redirect('/main/pay.html');
    res.render('login', {layout: null});
  });

  /*app.get('/login', function (req, res) {
    res.render('login', {layout: null});
  });*/
  app.get('/login1', function (req, res) {
    var sn = req.query.sn;
    var totalPrice = req.query.totalPrice;
    var amount = req.query.amount;
    res.render('login1', {layout: null,sn:sn,totalPrice:totalPrice,amount:amount});
  });

  app.get('/error', function (req, res) {
    res.render('error', {layout: null});
  });

  app.get('/add', function (req, res) {
    res.render('add', {layout: null});
  });

  app.get('/result', function (req, res) {
    res.render('result', {layout: null});
  });

  //  查询资料
  app.get('/orderlist/login', function (req, res) {
    res.render('orderlist/login', {layout: null});
  });
  app.get('/orderlist', orderListAp.index);
  app.post('/orderlist', orderListAp.index);

  // 用戶查詢訂單
  app.get('/orderFind', function (req, res) {
    res.render('orderfind', {layout: null});
  });
  app.get('/findOneOrder', function (req, res) {
    res.render('userorder', {layout: null});
  });

  //商品詳細查詢
  app.get('/yuanta/purchaseDetail',function(req,res){
    res.render('purchaseDetail',{layout: null})
  })
  app.get('/yuanta/purchaseDetailFromTb',function(req,res){
    res.render('purchaseDetailFromTb',{layout: null,detailArr:req.query.detailArr,totalPrice:req.query.totalPrice})
  })

  app.post('/yuanta/login', api.login);
  app.post('/htc-one/initData', api.initData);
  app.post('/htc-one/add', api.add);

  // 查詢訂單登陸
  app.post('/htc-one/loginQuery', orderListAp.loginQuery);
  app.post('/htc-one/queryOrder', api.queryOrder);

  app.post('/htc-one/initBack', api.initBack);

  // 刪除訂單
  app.post('/delOrder', orderListAp.delOrder);

  // 更新訂單
  app.post('/updateOrder', orderListAp.updateOrder);

  // 發送催款mail
  app.post('/sendMail', orderListAp.sendMail);


  //  導出Excel
  app.get('/importExcel', excel.orderImportExcel);

  //  匯入Excel
  app.post('/payImportData', excel.payImportData);
  app.get('/payorderlist', payListAp.queryPayOrder);
  app.post('/payorderlist', payListAp.queryPayOrder);
  // 更新
  app.post('/updatePay', payListAp.updatePay);

   // 二次对账
   app.post('/twoPay', excel.twoPay);
  // 商品管理
  app.get('/goodslist', goodsListAp.queryGoods);
  app.post('/goodslist', goodsListAp.queryGoods);

  // 商品添加
  app.post('/addGoods', goodsListAp.addGoods);

  // atm转账地址列印
  app.post('/addressPrint', print.addressPrint);
  app.get('/print', function (req, res) {
    res.render('atmprint/print', {layout: null});
  });

  // 虛擬賬號管理
  /*app.post('/htc-one/ResultVirtual', virtual.ResultVirtual);
  app.get('/virtual/result', function (req, res) {
    res.render('virtual/result', {layout: null});
  });

  app.get('/virtual/redirect', function (req, res) {
    res.render('virtual/redirect', {layout: null});
  });

  app.post('/htc-one/AtmVirtual', virtual.atmVirtual);*/

};
