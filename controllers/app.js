/**
 * Created with JetBrains WebStorm.
 * User: shihengwen
 * Date: 2013/11/21
 * Time: 下午 4:21
 * To change this template use File | Settings | File Templates.
 */

var mysql = require("../models/basedao");
var mail_ctrl = require("../models/mail");
//var mail1 = require("../models/email");
var config = require('../config').config;
var log = require('../util/log4js').logger('controllers/app'); /*require('../util/logger').getLogger();*/


var inSql = "insert into BuyInfo(USER_NAME,TEL,EMAIL,GOODS_SN,GOODS_IMEI,BUY_TYPE,INVOICE_TYPE,BUY_TIME," +
  "ACCOUNT_NO,COMPANY_NAME,COMPANY_NO,BUY_ADDRESS,BUY_ORDERNO,ACCOUNT_TYPE,BUY_STATUS,PAY_STATUS) ";

var errorMsg = "";


exports.login = function (req, res, next){
  var form = req.body || '';
//  var imei = form.txtIMEI;
  var sn = form.txtSerialNo;
  log.info("############################ 商品序號驗證 ######################");
  log.info("客戶端IP: "+req.connection.remoteAddress);
  log.info("傳入參數: sn=" +sn);

  var sqls = "SELECT count(*) count,GOODS_STATUS state FROM GoodsInfo WHERE GOODS_SN='"+sn + "'";
  log.info("sql = " + sqls);
  mysql.find(sqls, function (err, results) {
    if (err) {
      log.info(err);
      return false;
    } else {
      log.info(results);
      var resJson = results[0];
      var key = resJson.count;
      var state = resJson.state;   // --1：未购买，2已购买
      if(key == '1'){
        if(state == '1'){
//          res.render('login1', {imei: '1', sn: sn, PubLocation: '/login1', ErrorMessage: '', layout: null});
          res.render('products',{layout:null, sn:sn});
        }else if(state == "2"){
          errorMsg = "很抱歉！您輸入的電子優惠價序號碼已進行過購買，煩請再次確定及重新輸入正確的電子優惠價序號碼";
          res.render('login', {PubLocation: '/', ErrorMessage: errorMsg, layout: null});
        }
      }else if(key == '0'){
        errorMsg = "很抱歉！您輸入的電子優惠價序號碼有誤，煩請再次確定及重新輸入正確的電子優惠價序號碼";
        res.render('login', {PubLocation: '/', ErrorMessage: errorMsg, layout: null});
      }
    }
  });
};

/**
 * 数据初始化
 * */

exports.initData = function initData(req, res, next){

//  var data = {name:'', tel:'', email:'', imei:'', sn:'', payment:'',
//    address:'', invoice:'', account:'', companyName:'', companyNo:'', orderId:'', account_type:'',
//    address1:'',address2:'',address3:'',address4:''};

  var data = initJson(req);
  data.time = new Date().Format("yyyy/MM/dd");
//  res.render('add', {imei: data.imei, sn: data.sn,orderid: '', time:data.time , name: data.name,
  res.render('add', {imei: 0, sn: data.sn,amount:data.amount,totalPrice:data.totalPrice,orderid: '', time:data.time , name: data.name,
    tel: data.tel, address: data.address,address1: data.address1,address2: data.address2,address3: data.address3,address4: data.address4,
    invoice: data.invoice, companyName: data.companyName, email:data.email, account:data.account, account_type:data.account_type,
    companyNo: data.companyNo, payment: data.payment,paymentnoo:data.account_type, PubLocation: '/add',
    ErrorMessage: '', layout: null});


};



/**
 * 保存购买信息到资料库
 * */
exports.add = function (req, res, next){

  log.info("############################ 保存用戶購買資料 ######################");
  log.info("客戶端IP: "+req.connection.remoteAddress);
  /*var form = req.body || '';
  var data = req.session.form.txtIMEI;*/

  var data = initJson(req);

  log.info("傳入參數: "+ JSON.stringify(data));
  console.log(data);
  var sql = "SELECT count(*) count FROM BuyInfo WHERE GOODS_SN='"+data.sn + "'";
  log.info("sql = "+ sql);
  mysql.find(sql, function (err, results) {
    if (err) {
      log.info(err);
      return false;
    } else {
      log.info(results);
      var count = results[0].count;
      if(count == "1"){
        res.render('add', {imei: data.imei, sn: data.sn,amount:data.amount,totalPrice:data.totalPrice,orderid: data.orderId, time: data.time, name: data.name,
          tel: data.tel, address: data.address,address1: data.address1,address2: data.address2,address3: data.address3,address4: data.address4,
          invoice: data.invoice, companyName: data.companyName, email:data.email, account:data.account, account_type:data.account_type,
          companyNo: data.companyNo, payment: data.payment,paymentnoo:data.account_type, PubLocation: '/add',
          ErrorMessage: data.name + '小姐/先生，您的訂單已提交成功，注意查收郵件。\\n請不要重複提交！', layout: null});
      } else if(count == "0"){
        try{
          if (!data.name || !data.tel || !data.sn ||!data.address) {
            return;
          } else {
            // 檢查訂單編號（年月日+0001）
            var order = "";
            mysql.find("SELECT max(BUY_ORDERNO) time FROM BuyInfo", function (err, results) {
              if (err) {
                log.info(err);
                return false;
              } else {
                log.info(results);
                var time = results[0].time;
                log.info(time);
                try{
                  if(null != time){
                    order = getOrder(time);
                  } else{
                    order = new Date().Format("yyyyMMdd") + "0001";
                  }
                  data.time = new Date().Format("yyyy/MM/dd");
                  data.orderId = order;
                  console.log(data);

                  if(data.payment == '1'){
                    data.account = "";
                    data.account_type = "";
                  }

                  if(data.invoice == '1'){
                    data.companyName = "";
                    data.companyNo = "";
                  }

                  //插入資料到buyinfo表中
                  var sql = inSql + "values('"+data.name+"','"+data.tel+"', '"+data.email+"','"+data.sn+"','"+ "0" +"','"+data.payment+"','"+data.invoice+"',?," +
                    "'"+data.account+"','"+data.companyName+"','"+data.companyNo+"','"+data.address+"','"+data.orderId+"','"+data.account_type+"'," +
                    "'1','1')";
                  log.info(sql);
                  mysql.process(sql, new Date());
                  //插入資料到buyinfo_detail中
                  var sqls = "insert into buyinfo_detail (BUY_ORDERNO,PROD_NAME) values ('" + data.orderId + "','" + data.detail + "')";
                  log.info(sqls);
                  mysql.process(sqls,null);

                  // 更新商品???已??
                  var upSql = "UPDATE GoodsInfo SET GOODS_STATUS='2' WHERE GOODS_SN='"+data.sn + "'";
                  mysql.process(upSql, null);

                  // 當資料成功寫入資料庫時，請發成功加購信件(資料庫的MAIL)給加購者
                  var useremail = (data.email).trim();
                  if(useremail != ""){
                    log.info("Mail發送："+useremail);
                    var json = {payment: data.payment, account: data.account, account_type: data.account_type, address: data.address, totalPrice:data.totalPrice};
                    log.info("發送数据json ："+JSON.stringify(json) + " ####orderId="+data.orderId + " ###name=" + data.name);
                    console.log( "發送数据json ："+JSON.stringify(json) + " ####orderId="+data.orderId + " ###name=" + data.name);
                    mail_ctrl.send_reset_pass_mail(useremail, data.orderId, data.name, json, function(err,success) {
                      if(err) {
                        console.log( "發生錯誤："+err);
                        log.info("發生錯誤："+err);
                      }
                    });
                  }

                  res.render('result', {orderid: data.orderId, time: new Date().Format("yyyy/MM/dd"), name: data.name, amount:data.amount,totalPrice:data.totalPrice,
                    tel: data.tel, address: data.address,address1: data.address1,address2: data.address2,address3: data.address3,
                    address4: data.address4,invoice: data.invoice, companyName: data.companyName, account:data.account, account_type:data.account_type,
                    companyNo: data.companyNo, payment: data.payment, PubLocation: '/result',
                    ErrorMessage: '', layout: null});
                  log.info("########################################  資料保存成功 #################################");

                }catch(e){
                  console.log( "發生錯誤！"+e);
                  log.info( "發生錯誤！"+e);
                }
              }
            });
          }

        }catch(e){
          console.log( "發生錯誤！"+e);
          log.info( "發生錯誤！"+e);
        }
      }
    }
  });
};


function initJson(req){
  var data = {};

  var form = req.body || '';
  data.orderId = form.orderId || '';
  data.name = form.txtUserName || '';
  data.tel = form.txtTel || '';
  data.email = form.txtEmail || '';
//  data.imei = form.txtIMEI || '';
  data.sn = form.txtSerialNo || '';
  data.payment = form.txtPayment || '';
  data.totalPrice = form.totalPrice || 0;
  data.amount = form.amount || 0;
  data.time = form.time || '';
  data.detail = form.detail || '';

  var address1 = form.DD_nation || '';
  var address2 = form.DD_area || '';
  var address3 = form.DD_Road || '';
  var address4 = form.txtAddress || '';
  data.address1 = address1;
  data.address2 = address2;
  data.address3 = address3;
  data.address4 = address4;

  data.address = address3 + address1 + address2  + address4;
  data.invoice = form.txtInvoice || '';

  data.account = form.account_No || '';
  data.account_type = form.accountType || '';
  data.companyName = form.companyName || '';
  data.companyNo = form.companyNo || '';

  return data;
}



/**
 * 用戶查詢個人订单
 * */
exports.queryOrder = function(req, res, next){
  var form = req.body || '';
  var name = form.txtUsername;
  var sno = form.txtSerialNo;

  log.info("############################ 用戶訂單查詢 ######################");
  log.info("客戶端IP: "+req.connection.remoteAddress);
  log.info("傳入參數: name="+name+" ### sn=" +sno);

//  var sqls = "SELECT * FROM BuyInfo WHERE USER_NAME='"+name+"' AND GOODS_SN='"+sno+"'";
  var sqls = "SELECT a.*,b.PROD_NAME from buyinfo a LEFT JOIN  buyinfo_detail b on a.buy_orderno = b.BUY_ORDERNO where a.USER_NAME = '" + name + "' and a.GOODS_SN = '" + sno + "'";
  log.info("sql = " + sqls);
  try{
    mysql.find(sqls, function (err, results) {
      if (err) {
        log.info(err);
        return false;
      } else {
        log.info(results);
        if(results.length == 1){
          var list = results[0];
          var express_time = "";
          if(list.EXPRESS_TIME){
            if(list.EXPRESS_TIME == '0000-00-00 00:00:00') {
              express_time = "";
            } else{
              express_time = list.EXPRESS_TIME.Format("yyyy/MM/dd");
            }
          }
          else{
            express_time = "";
          }
          if(list.PROD_NAME){
            list.PROD_NAME = JSON.parse(list.PROD_NAME)
          }
          res.render('userorder', {orderid: list.BUY_ORDERNO, time: list.BUY_TIME.Format("yyyy/MM/dd"), name: list.USER_NAME,totalPrice: list.PROD_NAME.totalPrice,amount:list.PROD_NAME.amount,detailArr:encodeURI(JSON.stringify(list.PROD_NAME.detailArr).replace(/"/g,"\\\"")),
            tel: list.TEL, address: list.BUY_ADDRESS, invoice: list.INVOICE_TYPE, companyName: list.COMPANY_NAME,
            companyNo: list.COMPANY_NO, payment: list.BUY_TYPE,paymentnoo:list.ACCOUNT_TYPE,
            buy_status:list.BUY_STATUS,express_time:express_time, PubLocation: '/findOneOrder',
            ErrorMessage: '', layout: null});
        } else {
          res.render('Info', {PubTitle: '', PubInfo: '資料不存在，請重新輸入。',
            PubLocation: '/orderFind', PubTimeout: 1500, layout: null});
        }
      }
    });
  }catch (e){
    log.info("發送錯誤：" +e);
  }
};



/**
 * 計算每個用戶訂單編號
 * */
function getOrder(str){
  var res = "";
  var time = str.substring(0, 8) ;
  var thisTime = new Date().Format("yyyyMMdd");
  console.log(thisTime);
  if(time == thisTime){
    var id =  (str.substring(8, str.length) - 0) + 1;
    var len = (id+"").length;
    for(a=0; a<(4-len); a++)
      res += 0;
    res = thisTime + res + id;
  }else if(time < thisTime){
    res = thisTime + "0001";
  }
  console.log(res);
  return res;
}

/**
 * 日期格式化
 * */
Date.prototype.Format = function(format){
  var o = {
    "M+" : this.getMonth()+1, //month
    "d+" : this.getDate(), //day
    "h+" : this.getHours(), //hour
    "m+" : this.getMinutes(), //minute
    "s+" : this.getSeconds(), //second
    "q+" : Math.floor((this.getMonth()+3)/3), //quarter
    "S" : this.getMilliseconds() //millisecond
  }

  if(/(y+)/.test(format)) {
    format = format.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length));
  }

  for(var k in o) {
    if(new RegExp("("+ k +")").test(format)) {
      format = format.replace(RegExp.$1, RegExp.$1.length==1 ? o[k] : ("00"+ o[k]).substr((""+ o[k]).length));
    }
  }
  return format;
};



