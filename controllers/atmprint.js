/**
 * Created with JetBrains WebStorm.
 * User: shihengwen
 * Date: 2013/12/10
 * Time: 下午 1:48
 * To change this template use File | Settings | File Templates.
 */
var log = require('../util/log4js').logger('controllers/atmprint');
var mysql = require("../models/basedao");


/**
 * 列印查询
 * */
exports.addressPrint = function(req, res, next){
  var form = req.body || '';
  var data = form.data || '';

  log.info("############################ atm地址列印 ######################");
  log.info("客戶端IP: "+req.connection.remoteAddress);
  log.info("傳入參數: data="+data);
  try{
    var data1 = JSON.parse(data);
    console.log(data1);
    res.render('atmprint/print', {PubLocation: '/print', Results: data1, layout: null});
  }catch (e){
    console.log(e);
  }
};
