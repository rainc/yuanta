﻿/**
 * config
 */

var path = require('path');

//var I18n = require('i18n-2');
//var i18n = new I18n({locales: ['zh_CN', 'en'], extension: '.json'});
//i18n.setLocale('en');

exports.config = {
  debug: false,
  name: 'yuanta',
  description: 'yuanta加價購後台查詢',
  version: '0.1.1',

//  // site settings
//  site_headers: [
//    '<meta name="author" content="EDP@TAOBAO" />'
//  ],
  //host: 'localhost.cnodejs.org',
  host: 'localhost',
  site_logo: '', // default is `name`
  site_navs: [
    // [ path, title, [target=''] ]
    [ '/about', '关于' ]
  ],
  site_static_host: '', // 静态文件存储域名
  site_enable_search_preview: false, // 开启google search preview
  site_google_search_domain:  'icc.org',  // google search preview中要搜索的域名

  upload_dir: path.join(__dirname, 'public/'/*, 'user_data', 'images'*/),

  db_host: '127.0.0.1',
  db_database: 'yuanta',
  db_user: 'root',
  db_passwd: 'wang1game',

  table_list_count: 20,

  session_secret: 'yuanta',
  auth_cookie_name: 'yuanta',
  port: 5000,      // 5500 5600 5000 yuanta

//  // 话题列表显示的话题数量
//  list_topic_count: 20,

  // site links
  site_links: [
    {
      'text': 'Node 官方网站',
      'url': 'http://nodejs.org/'
    },
    {
      'text': 'Node Party',
      'url': 'http://party.cnodejs.net/'
    }
  ],

  // sidebar ads
  side_ads: [
    {
      'url': 'http://www.upyun.com/?utm_source=nodejs&utm_medium=link&utm_campaign=upyun&md=nodejs',
      'image': 'http://site-cnode.b0.upaiyun.com/images/upyun_logo.png',
      'text': ''
    },
    {
      'url': 'http://ruby-china.org/?utm_source=nodejs&utm_medium=link&utm_campaign=upyun&md=nodejs',
      'image': 'http://site-cnode.b0.upaiyun.com/images/ruby_china_logo.png',
      'text': ''
    }
  ],

  // mail SMTP
  mail_port: 25,
  mail_user: 'customer',
  mail_pass: 'SRHTC80661827',
  mail_host: 'mail.sristc.com',
  mail_sender: 'customer@sristc.com',
  mail_use_authentication: true,
  

  donwload_path: '/download',

  excel_path: 'E:/NodeJs/HTC_20131119/htc-one/',

//  i18n: i18n

  // excel 信息
  sheet:["消費者","電話","Email","商品序號S/N","商品IMEI","付款方式","發票","轉帳帳號後五碼","訂購日期","公司抬頭",
    "公司統一編號","送貨地址","郵地區號","出貨方式","送貨單號","送貨日期","發票號碼","付款狀態","訂單編號","轉帳帳戶銀行名称",
    "訂單狀態","備註"]

};
