/**
 * Created with JetBrains WebStorm.
 * User: hongxin
 * Date: 13-1-12
 * Time: 下午5:02
 * To change this template use File | Settings | File Templates.
 */
var mysql1 = require("../models/basedao");

var config = require('../config').config;
var check = require('validator').check,
  sanitize = require('validator').sanitize;
var EventProxy = require('eventproxy').EventProxy;

var log = require('../util/log4js').logger('controllers/goods');

exports.queryPayOrder = function (req, res, next) {

  if (!req.session.auth) {
    res.render('Info', {PubTitle: '', PubInfo: '請login。',
      PubLocation: '/orderlist/login', PubTimeout: 1500, layout: null});

  }


  var page = parseInt(req.query.page, 10) || 1;
  var keyword = req.query || '';

  var orderNo = req.query.orderNo || '';
  var buy_type = req.query.buy_type || '';
  var express_no = req.query.express_no || '';
  var dates = req.query.dates || '';
  var pay_order_status = req.query.pay_order_status || '';
  var account_no = req.query.account_no || '';


  var limit = config.table_list_count;
  var dblimit = ' LIMIT ' + (page-1) * limit + ',' + limit;

  var action = req.query.action || '';
  if (action== 'querybyid') {
    var id = req.query.id || '';
    query_one(id);
  } else {
    var events = [ 'iologs', 'pages'];
    var ep = EventProxy.create(events, function (iologs, pages) {
      res.render('payorder/index', {
        iologs:iologs,
        current_page:page,
        pages:pages,
        keyword:keyword,
        layout: 'layout2'
      });
    });

    ep.on('error', function (err) {
      ep.unbind();
      res.render('Info', {PubTitle: '', PubInfo: err.message, PubLocation: '/payorderlist', PubTimeout: 2000, layout: null});
      return;
    });

    var where = '';
    if (orderNo.length > 0) where += ' and BUY_ORDER=\'' + orderNo + '\'';
    if (buy_type.length > 0) where += ' and BUY_TYPE=\'' + buy_type + '\'';
    if (express_no.length > 0) where += ' and EXPRESS_NO=\'' + express_no + '\'';
    if (dates.length > 0) where += ' and DATE_FORMAT(PAY_TIME,\'%Y/%c/%d\')=\'' + dates + '\'';
    if (pay_order_status.length > 0) where += ' and PAY_ORDER_STATUS=\'' + pay_order_status + '\'';
    if (account_no.length > 0) where += ' and ACCOUNT_NO=\'' + account_no + '\'';

    if (where.length > 0) where = ' where' + where.substr(4);

    var sqls = "SELECT PAY_ID, BUY_ORDER, DATE_FORMAT(PAY_TIME,'%Y/%c/%d') PAY_TIME,EXPRESS_NO,ACCOUNT_NO,PAY_MONEY,PAY_ORDER_STATUS,BUY_TYPE,REMARK" +
      " FROM PAY_ORDER" + where + " ORDER BY PAY_ID DESC";
    sqls += dblimit;

    console.log(sqls);
    mysql1.find(sqls, function (err, iologs) {
      if (err) {
        return ep.emit('error', err);
      }
      ep.emit('iologs', iologs);
    });

    sqls = 'SELECT count(*) sumsize FROM PAY_ORDER' + where;
    mysql1.find(sqls, function(err,data){
      if (err) {
        return ep.emit('error', err);
      }
      var pages = Math.ceil(data[0].sumsize / limit);
      ep.emit('pages', pages);
    });
  }

  function query_one(id) {
    sqls = "SELECT PAY_ID, BUY_ORDER, DATE_FORMAT(PAY_TIME,'%Y/%c/%d') PAY_TIME,EXPRESS_NO,ACCOUNT_NO,PAY_MONEY,PAY_ORDER_STATUS,BUY_TYPE,REMARK FROM PAY_ORDER where PAY_ID='"+id+"'";
    console.log(sqls);
    mysql1.find(sqls, function (err, results) {
      if (err) {
        console.log('查询出错。'+err);
        log.info('查询出错。'+err);
        return;
      }
      console.log(results);
      try{
        res.json({ID: id,  Result: 0, ResultData: results});
      }catch (e){}

    });
  }
};


/**
 * 更新订单
 * */
exports.updatePay = function(req, res, next){

  log.info("############################ 更新對賬資料 ######################");
  log.info("客戶端IP: "+req.connection.remoteAddress);

  var obj = req.body;
  var pay_id = obj.pay_id || '';
  var buy_orderno = obj.buy_orderno || '';
  var pay_money = obj.pay_money || '';
  var payorderstatus = obj.payorderstatus || '';
  var remark = obj.remark || '';

  log.info("傳入參數: "+ JSON.stringify(obj));
  try{
    var sql = "UPDATE PAY_ORDER SET BUY_ORDER='"+buy_orderno+"'," +
      "PAY_ORDER_STATUS='"+payorderstatus+"',REMARK='"+remark+"'"+",PAY_MONEY='"+pay_money+"' WHERE PAY_ID='"+pay_id+"'";
    log.info( "sql="+sql);
    console.log( "sql="+sql);
//    if(express_time){
      try{
        mysql1.process(sql);
      }catch (e){}
//    }else{
//      mysql1.process(sql, '0000-00-00 00:00:00');
//    }
    res.json({ID: '', Result: 0, ResultData: ''});
  }catch(e){
    log.info( "發生錯誤！"+e);
    res.json({ID: '',  Result: 1, ResultData: ''});
  }
};




