/**
 * Created with JetBrains WebStorm.
 * User: hongxin
 * Date: 13-4-7
 * Time: 上午10:31
 * To change this template use File | Settings | File Templates.
 */

var fs = require('fs');
var Logger = require("./log");

var log = null;

var getLogger = function() {
  if (log != null) return log;
  // htc-one/
  var dss_proxyLogfile = fs.createWriteStream('htc.log', {flags: 'a'});
  log = new Logger(Logger.DEBUG, dss_proxyLogfile);

  return log;
};

exports.getLogger = getLogger;
