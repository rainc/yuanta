var mailer = require('nodemailer');
var config = require('../config').config;
var EventProxy = require('eventproxy').EventProxy;

var log = require('../util/log4js').logger('controllers/app');
//var log = require('../util/logger').getLogger();


mailer.SMTP = {
  host: config.mail_host,
  port: config.mail_port,
  use_authentication: config.mail_use_authentication,
  user: config.mail_user,
  pass: config.mail_pass
};

var SITE_ROOT_URL = 'http://' + config.hostname + (config.port !== 80 ? ':' + config.port : '');

/**
 * keep all the mails to send
 * @type {Array}
 */
var mails = [];
var timer;
/**
 * control mailer
 * @type {EventProxy}
 */
var mailEvent = new EventProxy();
/**
 * when need to send an email, start to check the mails array and send all of emails.
 */
mailEvent.on("getMail", function () {
  if (mails.length === 0) {
    return;
  } else {
    //遍历邮件数组，发送每一封邮件，如果有发送失败的，就再压入数组，同时触发mailEvent事件
    var failed = false;
    for (var i = 0, len = mails.length; i < len; ++i) {
      var message = mails[i];
      mails.splice(i, 1);
      i--;
      len--;
      var mail;
      try {
        message.debug = false;
        mail = mailer.send_mail(message, function (error, success) {
          if (error) {
            mails.push(message);
            failed = true;
          }
        });
      } catch(e) {
        mails.push(message);
        failed = true;
      }
      if (mail) {
        var oldemit = mail.emit;
        mail.emit = function () {
          oldemit.apply(mail, arguments);
        };
      }
    }
    if (failed) {
      clearTimeout(timer);
      timer = setTimeout(trigger, 60000);
    }
  }
});

/**
 * trigger email event
 * @return {[type]}
 */
function trigger() {
  mailEvent.trigger("getMail");
}

/**
 * send an email
 * @param  {mail} data [info of an email]
 */
function send_mail(data) {
  log.info("Email發送數據 Data = " + JSON.stringify(data));
  if (!data) {
    return;
  }
  if (config.debug) {
    console.log('******************** 在测试环境下，不会真的发送邮件*******************');
    for (var k in data) {
      console.log('%s: %s', k, data[k]);
    }
    return;
  }
  mails.push(data);
  trigger();
}

function send_active_mail(who, token, name, email, cb) {
  var sender =  config.mail_sender;
  var to = who;
  var subject = config.name + '社区帐号激活';
  var html = '<p>您好：<p/>' +
    '<p>我们收到您在' + config.name + '社区的注册信息，请点击下面的链接来激活帐户：</p>' +
    '<a href="' + SITE_ROOT_URL + '/active_account?key=' + token + '&name=' + name + '&email=' + email + '">激活链接</a>' +
    '<p>若您没有在' + config.name + '社区填写过注册信息，说明有人滥用了您的电子邮箱，请删除此邮件，我们对给您造成的打扰感到抱歉。</p>' +
    '<p>' +config.name +'社区 谨上。</p>';
  var data = {
    sender: sender,
    to: to,
    subject: subject,
    html: html
  };
  cb (null, true);
  send_mail(data);
}

/**
 *購買資料保存成功mail
 * */
function send_reset_pass_mail(who, token, name, json, cb) {

  log.info("##########################  进入 send_reset_pass_mail  ##############################");

  log.info("who="+who +" token="+token+" name="+name + " json = " + JSON.stringify(json));
  var sender = config.mail_sender;
  var to = who;
  var subject = '元大銀行活動';

  var msg, msg1 = "";
  if(json.payment == "1"){
    msg = '<p>付款方式：貨到付款</p>' +
      '<p>總價 : ' + json.totalPrice + ' </p>' +
      '<p>貨品收件地址 : ' +json.address+'</p>';
  } else if(json.payment == "2"){
    msg ='<p>付款方式：ATM轉帳</p>' +
      '<p>轉帳帳號後五碼 : ' +json.account+' (轉帳帳戶銀行名稱 : ' +json.account_type+')</p>' +
      '<p>總價 : ' + json.totalPrice + ' </p>' +
      '<p>貨品收件地址 : ' +json.address+'</p>' +
      '<p>轉入銀行 : 806  (元大銀行)</p>' +
      '<p>轉入帳號 : 20842000024331 </p>';

    msg1 = "您於匯款完成後，";
  } else if(json.payment == "3"){
    msg ='<p>付款方式：WebATM轉帳</p>' +
      '<p>轉帳帳號後五碼 : ' +json.account+' (轉帳帳戶銀行名稱 : ' +json.account_type+')</p>' +
      '<p>總價 : ' + json.totalPrice + ' </p>' +
      '<p>貨品收件地址 : ' +json.address+'</p>' +
      '<p>轉入銀行 : 806  (元大銀行)</p>' +
      '<p>轉入帳號 : 20842000024331 </p>';

    msg1 = "您於匯款完成後，";
  }

  var html = '<p>親愛的' + name + '小姐/先生您好：<p/>' +
    '<p>您SKII愛美優惠卷活動訂購 </p>' +
    '<p>訂單號 ' + token + '</p>' +
    msg +
    '<p>'+msg1+'優惠購商品將於十個工作日內寄出。謝謝您。</p>' +
    '<p>如有疑問，可洽詢訂購專線02-2558-3138。</p>';

  var data = {
    sender: sender,
    to: to,
    subject: subject,
    html: html
  };

  cb (null, true);
//  send_mail(data);
  mailer.send_mail(data, function(error){
    if(error){
      console.log('發送錯誤');
      console.log(error.message);
      log.info('發送錯誤');
      log.info('error.message');
      return;
    } else {
      console.log('Message sent successfully!');
    }
  });

}


/**
 *催款mail
 * */
function send_reset_pass_mail_(json, cb) {
  var sender = config.mail_sender;
  var to = json.email;
  var subject = 'HTC One max $999加價購HTC BoomBass無線重低音喇叭';

  var paymethod = "";
  if(json.payment == "2"){
    paymethod = "ATM轉帳";
  } else if(json.payment == "3"){
    paymethod = "WebATM轉帳";
  }

  var html = '<p>親愛的' + json.username + '小姐/先生您好：<p/>' +
    '<p>以下訂單尚未收到您的轉帳款項, 煩請您撥冗轉帳, 以便盡</p>' +
    '<p>速安排您的訂購貨品寄送事宜!</p>' +
    '<p>=============================================</p>' +
    '<p>親愛的' + json.username + '小姐/先生您好：<p/>' +
    '<p>您HTC One max加價購的BoomBass無線重低音喇叭</p>' +
    '<p>訂單號 ' + json.order_id + '</p>' +
    '<p>付款方式：' + paymethod + '</p>' +
    '<p>轉帳帳號後五碼 : ' +json.account+' (轉帳帳戶銀行名稱 : ' +json.account_type+')</p>' +
    '<p>單價 : $999</p>' +
    '<p>貨品收件地址 : ' +json.address+'</p>' +
    '<p>轉入銀行 : 808  (玉山銀行)</p>' +
    '<p>轉入帳號 : ' +json.virtual_account+'</p>' +
    '<p>您於匯款完成後，加價購商品將於十個工作日內寄出。謝謝您。</p>' +
    '<p>如有疑問，可洽詢訂購專線02-2558-3138。</p>' +
    '<p style="font-size: 15px;font-weight: bold;">恭賀新禧！親愛的加價購客戶，1月29日（三）起至2月4日（二）逢農曆</p>' +
    '<p style="font-size: 15px;font-weight: bold;">春節連續休假，2月5日（三）恢復正常上班。 春節期間如有 One Max 加</p>' +
    '<p style="font-size: 15px;font-weight: bold;">價購Boombass活動服務需要，可透過服務信箱 customer@sristc.com 詢問</p>' +
    '<p style="font-size: 15px;font-weight: bold;">，我們將於上班後儘速為您服務，不便之處，敬請見諒。</p>';

  var data = {
    sender: sender,
    to: to,
    subject: subject,
    html: html
  };

  cb (null, true);
//  send_mail(data);
  mailer.send_mail(data, function(error){
    if(error){
      console.log('發送錯誤');
      console.log(error.message);
      log.info('發送錯誤');
      log.info('error.message');
      return;
    } else {
      console.log('Message sent successfully!');
    }
  });
}

function send_reply_mail(who, msg) {
  var sender =  config.mail_sender;
  var to = who;
  var subject = config.name + ' 新消息';
  var html = '<p>您好：<p/>' +
    '<p>' +
    '<a href="' + SITE_ROOT_URL + '/user/' + msg.author.name + '">' + msg.author.name + '</a>' +
    ' 在话题 ' + '<a href="' + SITE_ROOT_URL + '/topic/' + msg.topic._id + '">' + msg.topic.title + '</a>' +
    ' 中回复了你。</p>' +
    '<p>若您没有在' + config.name + '社区填写过注册信息，说明有人滥用了您的电子邮箱，请删除此邮件，我们对给您造成的打扰感到抱歉。</p>' +
    '<p>' + config.name +'社区 谨上。</p>';

  var data = {
    sender: sender,
    to: to,
    subject: subject,
    html: html
  };

  send_mail(data);

}

function send_at_mail(who, msg) {
  var sender =  config.mail_sender;
  var to = who;
  var subject = config.name + ' 新消息';
  var html = '<p>您好：<p/>' +
    '<p>' +
    '<a href="' + SITE_ROOT_URL + '/user/' + msg.author.name + '">' + msg.author.name + '</a>' +
    ' 在话题 ' + '<a href="' + SITE_ROOT_URL + '/topic/' + msg.topic._id + '">' + msg.topic.title + '</a>' +
    ' 中@了你。</p>' +
    '<p>若您没有在' + config.name + '社区填写过注册信息，说明有人滥用了您的电子邮箱，请删除此邮件，我们对给您造成的打扰感到抱歉。</p>' +
    '<p>' +config.name +'社区 谨上。</p>';

  var data = {
    sender: sender,
    to: to,
    subject: subject,
    html: html
  };

  send_mail(data);
}

exports.send_active_mail = send_active_mail;
exports.send_reset_pass_mail = send_reset_pass_mail;
exports.send_reply_mail = send_reply_mail;
exports.send_at_mail = send_at_mail;

exports.send_reset_pass_mail_ = send_reset_pass_mail_;
