/**
 * Created with JetBrains WebStorm.
 * User: shihengwen
 * Date: 2013/12/5
 * Time: 下午 2:01
 * To change this template use File | Settings | File Templates.
 */
var log4js = require('log4js');

log4js.configure({
  appenders: [
    {
      type: "dateFile",
      pattern: "_yyyy-MM-dd", // hh mm ss sss
      filename: 'logs/yuanta.log',
      maxLogSize: 1024,
      alwaysIncludePattern: false, // true 只產生一文件
      backups: 4/*,
      category: 'logger'*/
    },
    /*{
      "type": "dateFile",
      "filename": "test.log",
      "pattern": "-yyyy-MM-dd",
      "alwaysIncludePattern": false
    },*/
    { type: 'console' }
  ],
  replaceConsole: true
});

// 加載log
//app.use(log4js.connectLogger(this.logger('normal'), {level:'auto', format:':method :url'}));

// var logger = log4js.getLogger('dateFile');
exports.logger=function(name){
  var logger = log4js.getLogger(name);
  /*FATAL	fatal errors are logged
  ERROR	errors are logged
  WARN	warnings are logged
  INFO	infos are logged
  DEBUG	debug infos are logged
  TRACE	traces are logged
  ALL  everything is logged*/

  logger.setLevel('INFO');

  return logger;
};
