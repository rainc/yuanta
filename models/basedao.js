/**
 * Created with JetBrains WebStorm.
 * User: shihengwen
 * Date: 2013/10/21
 * Time: 上午 9:28
 * To change this template use File | Settings | File Templates.
 */

var db = require('./db_pool');


/**
 * 增、删、改处理
 * */
function process(sql, params){
    var boo = false;
    db.getPoolConn().getConnection(function (err, conn) {
        if (err) console.log("POOL ==> " + err);
        conn.beginTransaction(function(err) {
            if (err) {
                console.log(err);
            }
            conn.query(sql, params, function(err, result) {
                if (err) {
                    conn.rollback(function() {
                        console.log(err);
                    });
                }else{
                    conn.commit(function(err) {
                        if (err) {
                            conn.rollback(function() {
                                console.log(err);
                            });
                        } else{
                            console.log('success!');
                            boo =true;
                        }
                    });
                }
            });
            close(conn);
            return boo;
        });
    });
}


/**
 * 查询
 * */
function find(sqls, cb){
    db.getPoolConn().getConnection(function (err, conn) {
        if (err) console.log("POOL ==> " + err);
        conn.query(sqls,function(err,rows){
            cb(err,rows);
            close(conn);
        });
    });
}


 /**
  * 释放连接
  * */
function close(conn){
     conn.release();
 }

exports.process = process;
exports.find = find;